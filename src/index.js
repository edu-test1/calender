import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import calenderReducer from './reducers'
import registerServiceWorker from './registerServiceWorker'
import './index.css'
import { Grid } from '@material-ui/core'
import App from './App'

const middleware = []
if (process.env.NODE_ENV !== 'production') {
	// console.log('.env', process.env.REACT_APP_NODE_ENV)
	middleware.push(createLogger())
}

//Redux store that holds the complete state tree of our app.
//The only way to change its state is by dispatching actions
const store = createStore(calenderReducer, applyMiddleware(...middleware))
// console.log('Index.js triggered')

ReactDOM.render(
	<Provider store={store}>
		<Grid item lg={12} sm={12} xs={12}>
			<App />
		</Grid>
	</Provider>,
	document.getElementById('root')
)

registerServiceWorker()
