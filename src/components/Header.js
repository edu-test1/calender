import React from 'react'
import { WEEK } from '../utils/constants'

const Header = () => {
	return (
		<div className='calendar__header'>
			{WEEK.map((name, index) => (
				<div key={name + '_' + index}>{name}</div>
			))}
		</div>
	)
}

export default Header
