import React from 'react'
import PropTypes from 'prop-types'
import { Tooltip } from '@material-ui/core'

const propTypes = {
	text: PropTypes.string,
	category: PropTypes.string,
	handleClick: PropTypes.func.isRequired
}

const ReminderLabel = ({ text, category, handleClick }) => {
	const labelClass = ['reminder__label']

	if (category.toLowerCase() === 'white')
		labelClass.push('reminder__label--work')
	if (category.toLowerCase() === 'salmon')
		labelClass.push('reminder__label--casual')
	if (category.toLowerCase() === 'grey')
		labelClass.push('reminder__label--calendar')

	return (
		<Tooltip title='Single Click to Edit' aria-label='add'>
			<div className={labelClass.join(' ')} onClick={handleClick}>
				<span>{text}</span>
			</div>
		</Tooltip>
	)
}

ReminderLabel.propTypes = propTypes

export default ReminderLabel
