import React, { useState } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import {
	Button,
	Grid,
	MenuItem,
	Select,
	TextField,
	Tooltip
} from '@material-ui/core'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import SaveIcon from '@material-ui/icons/Save'
import { REMINDER_TYPE } from '../utils/constants'

const ReminderForm = props => {
	// console.log('ReminderForm ', props)
	// const [text, setText] = useState(props.text)
	const [category, setCategory] = useState(props.category)
	// const [active, setActive] = useState()
	const [editing, setEditing] = useState(props.editing)
	const [date, setDate] = useState()
	const [startTime, setStartTime] = useState(props.startTime)
	const [endTime, setEndTime] = useState(props.endTime)

	const handleSubmit = event => {
		event.preventDefault()
		const updatedReminder = {
			category,
			updateTime: moment(),
			newReminder: false,
			open: false
		}
		setEditing(false)
		props.onSave(updatedReminder)
	}

	const handleStartTimeChange = e => {
		if (e.target.value < moment().format('HH:mm')) return

		let timeArray = e.target.value.split(':')
		const hours = timeArray[0]
		const minutes = timeArray[1]

		const updatedStartDate = props.date.set({
			hour: hours,
			minute: minutes
		})

		setStartTime(e.target.value)
		setEndTime(`${parseInt(hours, 10) + 1}:${minutes}`)
		setDate(updatedStartDate)
	}

	const handleEndTimeChange = e => {
		if (e.target.value < moment().format('HH:mm')) return

		let timeArray = e.target.value.split(':')
		const hours = timeArray[0]
		const minutes = timeArray[1]

		const updatedEndDate = props.date.set({
			hour: hours,
			minute: minutes
		})

		setEndTime(`${parseInt(hours, 10) - 1}:${minutes}`)
		setDate(updatedEndDate)
	}

	if (props.editing === false) return null

	return (
		<form className='reminder' onSubmit={handleSubmit}>
			<Grid item lg={12} sm={12}>
				<Grid item lg={12} sm={12}>
					<TextField
						type='string'
						required
						placeholder={props.placeholder || 'Enter new reminder...'}
						autoFocus={true}
						defaultValue={props.text}
						inputProps={{ maxLength: 20 }}
						onChange={props.onChange}
					/>
				</Grid>
				<Grid item lg={4} sm={4}>
					<Select onChange={props.onCategoryChange} value={props.category}>
						{REMINDER_TYPE.map((rt, idx) => {
							return (
								<MenuItem key={idx + '_' + rt} value={rt}>
									{rt}
								</MenuItem>
							)
						})}
					</Select>
				</Grid>
			</Grid>
			<Grid style={{ whiteSpace: 'pre-wrap' }}>
				<div>
					<span>Start</span>&nbsp;&nbsp;
					<TextField
						type='time'
						required
						defaultValue={startTime}
						onChange={handleStartTimeChange}
					/>
				</div>
				<div style={{ whiteSpace: 'pre-wrap' }}>
					<span>End</span>&nbsp;&nbsp;
					<TextField
						type='time'
						required
						defaultValue={endTime}
						onChange={handleEndTimeChange}
					/>
				</div>
			</Grid>
			<Grid style={{ whiteSpace: 'pre-wrap' }}>
				<Button
					variant='outlined'
					color='secondary'
					size='small'
					type='button'
					onClick={props.onDelete}>
					<Tooltip title='Delete Reminder' aria-label='add'>
						<DeleteOutlineIcon />
					</Tooltip>
				</Button>
				&nbsp;
				<Button size='small' color='primary' variant='outlined' type='submit'>
					<Tooltip title='Save Reminder' aria-label='add'>
						<SaveIcon />
					</Tooltip>
				</Button>
			</Grid>
		</form>
	)
}

ReminderForm.propTypes = {
	onSave: PropTypes.func.isRequired,
	text: PropTypes.string,
	placeholder: PropTypes.string,
	editing: PropTypes.bool,
	newTodo: PropTypes.bool
}

ReminderForm.defaultProps = {
	text: '',
	category: 'home',
	startTime: `${moment().format('HH')}:${moment().minutes()}`,
	endTime: `${moment().add(1, 'hours').format('HH')}:${moment().minutes()}`
}

export default ReminderForm
