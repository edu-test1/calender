import React from 'react'
import PropTypes from 'prop-types'
import { Grid, IconButton, Tooltip } from '@material-ui/core'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore'

const propTypes = {
	currentMonthTitle: PropTypes.string.isRequired,
	nextMonthAction: PropTypes.func.isRequired,
	prevMonthAction: PropTypes.func.isRequired
}

const RouteAction = ({
	currentMonthTitle,
	nextMonthAction,
	prevMonthAction,
	toDisablePrev
}) => {
	return (
		<Grid className='calendar__nav'>
			<h2>{currentMonthTitle}</h2>
			<Grid>
				<IconButton
					color='secondary'
					disabled={toDisablePrev}
					onClick={prevMonthAction}>
					<Tooltip title='Go To Previous Month' aria-label='add'>
						<NavigateBeforeIcon />
					</Tooltip>
				</IconButton>
				&nbsp;&nbsp;&nbsp;
				<IconButton color='primary' onClick={nextMonthAction}>
					<Tooltip title='Go To Next Month' aria-label='add'>
						<NavigateNextIcon />
					</Tooltip>
				</IconButton>
			</Grid>
		</Grid>
	)
}

RouteAction.propTypes = propTypes

export default RouteAction
