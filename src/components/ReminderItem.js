import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Grid } from '@material-ui/core'
import ReminderForm from './ReminderForm'
import ReminderLabel from './ReminderLabel'

const ReminderItem = props => {
	// console.log('ReminderItem ', props)
	const [text, setText] = useState(props.reminder.text)
	const [category, setCategory] = useState(props.reminder.category)
	const [active, setActive] = useState(false)
	const [editing, setEditing] = useState(props.reminder.newReminder)
	const [date, setDate] = useState(props.reminder.date)
	const [startTime, setStartTime] = useState(props.reminder.startTime)
	const [endTime, setEndTime] = useState(props.reminder.endTime)

	const handleSave = formFields => {
		const updatedReminder = {
			...props.reminder,
			...formFields,
			text: text,
			category: category
		}
		props.editReminder(props.weekIndex, props.weekdayIndex, updatedReminder)
		setEditing(false)
	}

	const handleCategoryChange = event => setCategory(event.target.value)

	const handleChange = e => setText(e.target.value)

	const handleClick = () => {
		setActive(!active)
		setEditing(true)
	}

	const deleteReminder = () =>
		props.deleteReminder(props.weekIndex, props.weekdayIndex, props.reminder)

	return (
		<Grid className='reminder'>
			{text && (
				<ReminderLabel
					text={text}
					category={category}
					handleClick={handleClick}
				/>
			)}
			<ReminderForm
				text={text}
				category={category}
				date={date}
				startTime={startTime}
				endTime={endTime}
				editing={editing}
				onChange={handleChange}
				onCategoryChange={handleCategoryChange}
				onSave={reminder => handleSave(reminder)}
				onDelete={deleteReminder}
			/>
		</Grid>
	)
}

ReminderItem.propTypes = {
	disabled: PropTypes.bool
}

ReminderItem.defaultProps = {
	text: 'New Reminder'
}

export default ReminderItem
