import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import ReminderItem from './ReminderItem'
import { Grid } from '@material-ui/core'

const DayBlock = props => {
	// console.log('DayBlock ', props)
	const today = moment()

	const handleDoubleClick = (weekIndex, weekdayIndex, weekdayDate) => {
		// If day is in the past, dont allow click
		if (today > weekdayDate && !today.isSame(weekdayDate, 'd')) return
		props.actions.addReminder(weekIndex, weekdayIndex)
	}

	const getDayClass = day => {
		const classes = ['week__day']

		if (today.isSame(day, 'd')) classes.push('week__day--today')

		if (today > day && !today.isSame(day, 'd')) classes.push('week__day--past')

		if (day.day() === 0 || day.day() === 6) classes.push('week__day--weekend')

		return classes.join(' ')
	}

	const renderWeeks = (week, index) => {
		const { month, actions } = props

		return month.map((week, index) => (
			<Grid
				container
				alignItems='baseline'
				justify='space-evenly'
				key={week.uuid}
				className='week'>
				{week.days.map((weekday, index) => (
					<Grid
						item
						xs
						key={weekday.uuid}
						className={getDayClass(weekday.date)}
						onDoubleClick={() =>
							handleDoubleClick(week.index, weekday.index, weekday.date)
						}>
						{weekday.date.format('D')}
						{weekday.reminders.map(reminder => (
							<ReminderItem
								key={reminder.uuid}
								reminder={reminder}
								weekIndex={week.index}
								weekdayIndex={weekday.index}
								editReminder={actions.editReminder}
								deleteReminder={actions.deleteReminder}
							/>
						))}
					</Grid>
				))}
			</Grid>
		))
	}

	return <div className='calendar__month'>{renderWeeks()}</div>
}

DayBlock.propTypes = {
	disabled: PropTypes.bool
}

export default DayBlock
