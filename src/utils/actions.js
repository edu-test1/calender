import {
	ADD_REMINDER,
	EDIT_REMINDER,
	DELETE_REMINDER,
	CALENDAR_PREV_MONTH,
	CALENDAR_NEXT_MONTH
} from './constants'

export const prevMonth = () => ({
	type: CALENDAR_PREV_MONTH
})

export const nextMonth = () => ({
	type: CALENDAR_NEXT_MONTH
})

export const addReminder = (weekIndex, weekdayIndex) => ({
	type: ADD_REMINDER,
	payload: { weekIndex, weekdayIndex }
	// payload: fetch('https://anyendpoint/users')
	// 	.then(response => response.json())
	// 	.catch(error => error)
})

export const editReminder = (weekIndex, weekdayIndex, reminder) => ({
	type: EDIT_REMINDER,
	payload: { weekIndex, weekdayIndex, reminder }
})

export const deleteReminder = (weekIndex, weekdayIndex, reminder) => ({
	type: DELETE_REMINDER,
	payload: { weekIndex, weekdayIndex, reminder }
})
