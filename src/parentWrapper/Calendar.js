import React, { useContext, useEffect } from 'react'
// import { CalenderContext } from '../App'
import Header from '../components/Header'
import Navigation from '../containers/Navigation'
import Month from '../containers/Month'
import { Grid } from '@material-ui/core'

const Calendar = () => {
	// const { calenderInfo, setCalenderInfo } = useContext(CalenderContext)

	// useEffect(() => {
	// 	setCalenderInfo({ payload: 'Hi' })
	// }, [])

	return (
		<Grid className='calendar'>
			<Navigation />
			<Header />
			<Month />
		</Grid>
	)
}

export default Calendar
