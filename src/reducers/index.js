import { combineReducers } from 'redux'
import calendarReducer from './calendar-reducer'

// Helper function that turns an object whose values are different reducing functions
// into a single reducing function you can pass to createStore.
const allReducers = combineReducers({
	calendar: calendarReducer
})

export default allReducers
