import moment from 'moment'
import { v4 as uuidv4 } from 'uuid'

/**
 * createCalendarMonth creates a new calender month
 * from the first week to the last in that month
 *
 */
const createCalendarMonth = (startWeek, endWeek) => {
	const monthArray = []

	for (
		let weekIndex = startWeek, weekArrayIndex = 0;
		weekIndex < endWeek + 1;
		weekIndex++, weekArrayIndex++
	) {
		const weekid = uuidv4()

		// Push a week object into the monthArray
		monthArray.push({
			uuid: weekid,
			weekIndex,
			index: weekArrayIndex,
			days: Array(7)
				.fill({ id: 0 }) // Fill the array with 7 blank days
				.map((item, index) => {
					return {
						uuid: uuidv4(),
						parentWeekId: weekid, // Keep a track of the parent
						date: moment() // Get todays date
							.week(weekIndex) // Get or sets the week of the year
							.startOf('week') // set to the first day of this week, 12:00 am
							.clone() // Create a clone of a duration. Durations are mutable - This will clone Sunday date
							.add(index + 1, 'day'), // Add certain amount/index days to the start of the week which will be a Monday
						weekIndex,
						index: index,
						reminders: [] // Create blank reminders within the day
					}
				})
		})
	}
	return monthArray
}

// Gets the current month start week index based on 52 weeks in a year
const initialStartWeek = moment().startOf('month').add(0, 'month').week()
// console.log('initialStartWeek ', initialStartWeek)
// Gets the current month end week index based on 52 weeks in a year
const initialEndWeek = moment().endOf('month').add(0, 'month').week()
// console.log('initialEndWeek ', initialEndWeek)
const currentMonth = createCalendarMonth(initialStartWeek, initialEndWeek)

// Initial State
const initialState = {
	currentMonthIndex: 0,
	month: currentMonth,
	year: { 0: currentMonth } // keep a track of the months in the year
}

// console.log('initialState ', initialState)

const calendarReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'CALENDAR_PREV_MONTH': {
			const prevMonthIndex = state.currentMonthIndex - 1
			const updatedStartWeek = moment()
				.startOf('month')
				.add(prevMonthIndex, 'month')
				.week()
			// console.log('updatedStartWeek ', updatedStartWeek)
			const updatedEndWeek = moment()
				.endOf('month')
				.add(prevMonthIndex, 'month')
				.week()
			// console.log('updatedEndWeek ', updatedEndWeek)

			const updatedYearCalendar = {
				...state.year,
				[state.currentMonthIndex]: state.month, // Save the current month
				[prevMonthIndex]: state.year[prevMonthIndex]
					? state.year[prevMonthIndex]
					: createCalendarMonth(updatedStartWeek, updatedEndWeek)
			}
			// console.log('updatedYearCalendar ', updatedYearCalendar)
			return {
				...state,
				currentMonthIndex: prevMonthIndex,
				month: updatedYearCalendar[prevMonthIndex],
				year: updatedYearCalendar
			}
		}
		case 'CALENDAR_NEXT_MONTH': {
			const nextMonthIndex = state.currentMonthIndex + 1
			const updatedStartWeek = moment()
				.startOf('month')
				.add(nextMonthIndex, 'month')
				.week()
			// console.log('updatedStartWeek ', updatedStartWeek)
			const updatedEndWeek = moment()
				.endOf('month')
				.add(nextMonthIndex, 'month')
				.week()
			// console.log('updatedEndWeek ', updatedEndWeek)

			const updatedYearCalendar = {
				...state.year,
				[state.currentMonthIndex]: state.month, // Save the current month
				[nextMonthIndex]: state.year[nextMonthIndex]
					? state.year[nextMonthIndex]
					: createCalendarMonth(updatedStartWeek, updatedEndWeek)
			}
			// console.log('updatedYearCalendar ', updatedYearCalendar)

			updatedYearCalendar[nextMonthIndex].map(next => {
				next.days.map(dayToUpdate => {
					return state.month.map(prev => {
						return prev.days.map(oldValue => {
							if (oldValue.reminders.length > 0) {
								if (oldValue.date.isSame(dayToUpdate.date))
									dayToUpdate.reminders = oldValue.reminders
								//merging reminders to next month
							}
						})
					})
				})
			})

			return {
				...state,
				currentMonthIndex: nextMonthIndex,
				month: updatedYearCalendar[nextMonthIndex],
				year: updatedYearCalendar
			}
		}
		case 'ADD_REMINDER': {
			const updatedMonth = state.month.map((week, index) => {
				if (action.payload.weekIndex === index) {
					const dayToUpdate = week.days[action.payload.weekdayIndex]

					dayToUpdate.reminders.push({
						text: '',
						date: moment(),
						category: 'default',
						open: true,
						newReminder: true,
						uuid: uuidv4(),
						parentDayUuid: week.days[action.payload.weekdayIndex].uuid,
						grandparentUuid: week.uuid
					})
				}

				return week
			})

			return {
				...state,
				month: updatedMonth
			}
		}
		case 'DELETE_REMINDER': {
			const updatedMonth = state.month.map((week, index) => {
				if (action.payload.weekIndex === index) {
					const dayToUpdate = week.days[action.payload.weekdayIndex]
					dayToUpdate.reminders = dayToUpdate.reminders.filter(
						reminder => reminder.uuid !== action.payload.reminder.uuid
					)
				}

				return week
			})

			return {
				...state,
				month: updatedMonth
			}
		}
		case 'EDIT_REMINDER': {
			const updatedMonth = state.month.map((week, index) => {
				if (action.payload.weekIndex !== index) {
					return week
				}

				const dayToUpdate = week.days[action.payload.weekdayIndex]
				dayToUpdate.reminders = dayToUpdate.reminders.map(reminder => {
					if (reminder.uuid !== action.payload.reminder.uuid) {
						return reminder
					}

					return {
						...reminder,
						...action.payload.reminder,
						updateTime: moment()
					}
				})

				return week
			})

			return {
				...state,
				month: updatedMonth
			}
		}
		default:
			return state
	}
}

export default calendarReducer
