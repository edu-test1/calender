import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as CalendarActions from '../utils/actions'
import DayBlock from '../components/DayBlock'

// console.log('Month Container')
//Returns a plain object that contains the data the component needs.
const mapStateToProps = state => ({
	month: state.calendar.month
})

//Here functions that should be talking to Redux is passed.
//These functions will be reachable from the props.
const mapDispatchToProps = dispatch => ({
	//Turns an object whose values are action creators, into an object with the same keys,
	//but with every action creator wrapped into a dispatch call so they may be invoked directly.
	actions: bindActionCreators(CalendarActions, dispatch)
})

//Method used to make a component connected to Redux. This returns a function
export default connect(mapStateToProps, mapDispatchToProps)(DayBlock)
