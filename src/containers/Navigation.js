import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import moment from 'moment'
import * as CalendarActions from '../utils/actions'
import RouteAction from '../components/RouteAction'

const Navigation = props => {
	// console.log('Navigation ', props)
	const { currentMonthIndex, actions } = props
	let toDisablePrev = false

	const currentMonthTitle = moment()
		.startOf('month')
		.add(currentMonthIndex, 'month')
		.format('MMMM YYYY')

	if (
		parseInt(currentMonthTitle.replace(/[^0-9]/g, '')) ===
		new Date().getFullYear()
	) {
		const month = currentMonthTitle.replace(/[^a-zA-Z]/g, '')
		if (month.toLowerCase() === 'january') toDisablePrev = true
	}

	return (
		<RouteAction
			nextMonthAction={actions.nextMonth}
			prevMonthAction={actions.prevMonth}
			currentMonthTitle={currentMonthTitle}
			toDisablePrev={toDisablePrev}
		/>
	)
}

//Use for the below functions are explained in Month.js
const mapStateToProps = state => ({
	currentMonthIndex: state.calendar.currentMonthIndex
})

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(CalendarActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Navigation)
